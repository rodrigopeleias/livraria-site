package br.com.casadocodigo.livraria.controller;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.casadocodigo.livraria.modelo.Acervo;

@Controller
public class HomeController {
	
	private Acervo acervo;
	private Result result;
	
	public HomeController(Acervo acervo, Result result) {
		this.acervo = acervo;
		this.result = result;
	}
	
	@Deprecated
	HomeController() {		
	}

	public void inicio() {
		this.result.include("livros", acervo.todosOsLivros());
	}
}
