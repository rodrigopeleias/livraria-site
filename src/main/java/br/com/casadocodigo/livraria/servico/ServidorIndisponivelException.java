package br.com.casadocodigo.livraria.servico;

public class ServidorIndisponivelException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServidorIndisponivelException(String url, Exception e) {
		super("Erro ao fazer requisição ao servidoe na url " + url, e);
	}

}
