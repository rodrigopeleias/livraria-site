package br.com.casadocodigo.livraria.servico;

import java.util.List;

import javax.inject.Inject;

import com.thoughtworks.xstream.XStream;

import br.com.caelum.vraptor.environment.Property;
import br.com.caelum.vraptor.serialization.xstream.XStreamBuilder;
import br.com.casadocodigo.livraria.modelo.Acervo;
import br.com.casadocodigo.livraria.modelo.Livro;

public class AcervoNoAdmin implements Acervo{
	
	private ClienteHTTP http;
	private XStreamBuilder builder;
	private String adminUrl;
	
	@Inject
	public AcervoNoAdmin(ClienteHTTP http, XStreamBuilder builder, @Property("admin.url") String adminUrl) {
		this.http = http;
		this.builder = builder;
		this.adminUrl = adminUrl;
	}
	
	@Deprecated
	AcervoNoAdmin() {	
	}

	@Override
	public List<Livro> todosOsLivros() {
		String xml = adminUrl + "/integracao/listaLivros";		
		
		XStream xstream = builder.xmlInstance();
		xstream.alias("livros", List.class);
		xstream.alias("livro", Livro.class);
		
		List<Livro> livros = (List<Livro>)xstream.fromXML(xml);
		return livros;
	}

}
