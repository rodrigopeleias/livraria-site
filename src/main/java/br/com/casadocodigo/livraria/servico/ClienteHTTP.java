package br.com.casadocodigo.livraria.servico;

public interface ClienteHTTP {
	String get(String url) throws ServidorIndisponivelException;
}
